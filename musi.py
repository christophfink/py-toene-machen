#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  musi.py -> try stupid stuff
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
from lib.musical_notes import notes
from lib.routing import fetchRoute
from lib.elev_profile import ElevationModel
from lib.wavefile_handler import WaveFile
from lib.signal_generator import compute_n_samples,compute_sample
from lib.encoder import encode

import argparse,random,sys
#from random import sample
		

def main():
	parser = argparse.ArgumentParser(description='Description of your program')
	parser.add_argument('-l','--duration', help='How long should the audio journey be approximately?', required=False, default=60, type=int, metavar='sec')
	parser.add_argument('-b','--bands', help="Reduce processing to n randomly chosen frequency bands", required=False, default=False, type=int, metavar='n')
	parser.add_argument('from',help="Where from would you like to travel?")
	parser.add_argument('to',help="… and where does your journey lead you?")
	parser.add_argument('-o','--output',help="wav file to write output to (default: ./from->to.wav)",required=False,default=False,metavar='filename')
	args = vars(parser.parse_args())

        route=fetchRoute(args['from'],args['to']) 
        if not route:
		sys.exit("Could not find a route from %s to %s." %(args['from'],args['to']))

	frequencies=list((n.frequency for n in notes))
	#frequencies=list((n.frequency for n in notes))[18:-18] # reduce spectrum a bit
	if args['bands']:
		frequencies=random.sample(frequencies,args['bands']) # only use random subset of half notes
	#frequencies=[440.0,1620.0]

	if not args['output']:
		 args['output']="%s->%s_%dbands.wav" %(args['from'],args['to'],len(frequencies))

	em=ElevationModel("/home/christoph/geodata/SRTM_V4/srtm.tif",route.extent)
        
        profiles=[]
	maxelev=0.0
	minelev=9999.9
        
	for p in route:
		(position,direction)=p
		profile=em.getPerpendicularProfile(position,direction,0.01,len(frequencies)*2) # n samples in 0.1deg distance from each other
		profiles.append(profile)
		maxelev=max((maxelev,float(max(profile))))
		minelev=min((minelev,float(min(profile))))

	#span=maxelev-minelev
	#expand=span/maxelev
	#for p in profiles:
	#	p=list((v-minelev)*expand for v in p)

	for p in range(len(profiles)):
		#print profiles[p]
		profiles[p]=list(i**10 for i in profiles[p])
		#print profiles[p]
		maxp=max(profiles[p])
		minp=min(profiles[p])
		span=maxp-minp
		if not span==0: 
			profiles[p]=list((v-minp)*span/maxp for v in profiles[p])
		#print profiles[p]
		#return
		
	em=None # free mem
        
	rate=44100.0
	dur=int(args['duration']*rate/len(profiles)) # only whole-number sample counts ;-) might make the overall file slightly shorter
	offset=1

	wavefile=WaveFile(args['output'])
	csvfile=open(args['output'].replace('.wav','')+'.csv','w')
	
	#wavefile_only440Hz=WaveFile(args['output']+"_440Hz",nchannels=1)

	#for p in profiles:
	#	csvfile.write(','.join(("%.4f" %(e) for e in p))+"\n")
	#	samples=compute_n_samples(frequencies,p,offset,dur)
	#	
	#	volume=max(p)/maxelev			
	#	samples=(v*volume for v in samples)
#
#		samples=((v,v) for v in samples)  # two identical stereo channels – this is only for the time being
#		wavefile.write_samples(samples)
#		offset+=dur
	for p in range(len(profiles)-1):
		csvfile.write(','.join(("%d" %(e) for e in profiles[p]))+"\n")

		this_profile=profiles[p]
		next_profile=profiles[p+1]

		for d in range(dur):
			progress=float(d)/dur  # distance to next profile 
			profile=list(this_profile[i]+(next_profile[i]-this_profile[i])*progress for i in range(len(this_profile)))  # linear interpolation
			#volume=max(profile)/maxelev*0.95 # 1.0 apparently is too much
			
			sampleL=compute_sample(frequencies,profile[:len(frequencies)],offset)
			#sampleL*=volume
			sampleR=compute_sample(list(reversed(frequencies)),profile[len(frequencies):],offset)
			#sampleR*=volume
			wavefile.write_samples(((sampleL,sampleR),))

			#print frequencies,profile

			#sample440=compute_sample(frequencies[12:12],profile[12:12])
			#wavefile_only440Hz.write_samples(((sample440,),))

			offset+=1
		#break


	wavefile=None
	csvfile.close()

	encode(
		args['output'],
		{
			"artist": "xf",
			"title": "%s to %s" %(args["from"],args["to"]),
			"album": "audiojourney13"
		}
	)
		


	return
	#####
	# further down only unsuccessful try-outs - which I don't want to delete already, but which are not executed either …

	frequencies=(n.frequency for n in notes)

	channels=(
		(sine_wave(25.0,amplitude=0.0),),
		(sine_wave(25.0,amplitude=0.0),)
		)
	samples=compute_samples(channels,length)
	frames=generate_frames(samples)
	offset+=length

	for n in range(len(notes)-1): #(15,15,15,15,15,15,15): #range(len(notes)-1):
		#print notes[n-1]
		#print notes[n]
		#print notes[n+1]
		#print "--"
		freq1=notes[n].frequency
#		freq2=notes[n+1].frequency

		channels=(
			(
				sine_wave(freq1,amplitude=0.2),
			),
			(
				sine_wave(freq1,amplitude=0.2),
			)
		)
		#l=length-(wl1-(length%wl1))
		l=int(int(rate/freq1)*freq1)
		samples=compute_samples(channels,l)
		frames+=generate_frames(samples)
		offset+=l

	write_prepared_frames_to_wavfile("test.wav",frames)
	#channels=((sine_wave(440.0, amplitude=0.1),),)
	#samples=compute_samples(channels)
	#print samples

if __name__ == '__main__':
        main()
