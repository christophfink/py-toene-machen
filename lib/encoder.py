#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  encodes to ogg and mp3
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

from subprocess import check_call 

def encode(wavf,id3tag,formats=["ogg","mp3"]):
	id3default={
		"artist": "",
		"title": "",
		"album": ""
	}
	id3default.update(id3tag)
	id3tag=id3default
	id3default=None

	for f in formats:
		if f=="ogg":
			check_call([
				"oggenc",
				"-q7", # VBR quality 7
				"--quiet", # hush
				"-a", id3tag["artist"],
				"-t", id3tag["title"],
				"-l", id3tag["album"],
				"-o", wavf.replace(".wav","")+".ogg",  # output file name
				wavf # input
			])
		if f=="mp3":
			check_call([
				"lame",
				"-q2", # overall quality setting 2
				"-v",  # use VBR
				"-V7", # VBR quality 7
				"-X0", # quantization search algorithm
				"--ta", id3tag["artist"],
				"--tt", id3tag["title"],
				"--tl", id3tag["album"],
				wavf, # input
				wavf.replace(".wav","")+".mp3",  # output file name
			])
		

def main():
	pass

if __name__ == '__main__':
        main()
