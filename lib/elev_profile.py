#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  elev_profile -> calculates elevation profile in point x,y perpendicular 
#  to direction dir (in radians), with width w
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

# a great deal of the following code is lazyily copied and slightly altered
# from http://gis.stackexchange.com/questions/29632/raster-how-to-get-elevation-at-lat-long-using-python
# (especially the helper functions, the rest is just coded along the ideas
# in this stackexchange-post

from osgeo import gdal
from math import pi,tan,sin,cos

class ElevationModel:
	def __init__(self,dem,extent):
		ds=gdal.Open(dem,gdal.GA_ReadOnly)
		if ds==None:
			raise Exception
		
		geotransform=ds.GetGeoTransform()
		if geotransform==None:
			self.ds=None
			raise Exception

		#(success,invgeotransform)=gdal.InvGeoTransform(geotransform)
		#if not success:
		#	ds=None
		#	raise Exception
		
		self.geotransform=geotransform
		#self.invgeotransform=invgeotransform
		geotransform=None
		#invgeotransform=None
		
		#print self.geotransform

		self.width=ds.RasterXSize
		self.height=ds.RasterYSize

		(minx,miny)=self.__mapToPixel((extent[0],extent[1]))
		(maxx,maxy)=self.__mapToPixel((extent[2],extent[3]))
		
		#print "extent: %d, %d = %.5f %.5f -> %d, %d = %.5f %.5f -- %d x %d " %(minx,miny,extent[0],extent[1],maxx,maxy,extent[2],extent[3],maxx-minx,maxy-miny)

		band=ds.GetRasterBand(1) # numbered from 1 … 8;7
		self.dem=band.ReadAsArray(int(minx),int(maxy),int(maxx-minx),int(miny-maxy))
		#self.dem=band.ReadAsArray(int(minx),int(maxy),int(maxx-minx),int(miny-maxy)) # note that for y min/max are exchanged -> gdal counts from top left, 
		#print(int(minx),int(miny),int(maxx-minx),int(miny-maxy))

		self.offset=(int(minx),int(maxy))
		#print "offset: %d %d" %(minx,maxy)

		#print "convert -verbose -monitor '%s' -crop %dx%d+%d+%d '%s'" %(dem,int(maxx-minx),int(miny-maxy),int(minx),self.height-int(maxy),"ausschnitt.tif")

		#### debug: save cut out extent ####
		#dr=gdal.GetDriverByName("GTiff")
		#dst=dr.Create("debug.tif",int(maxx-minx),int(miny-maxy),1,gdal.GDT_Int16)
		#dst.GetRasterBand(1).WriteArray(self.dem)
		#dst=None

		band=None
		ds=None

	def __del__(self):
		self.dem=None

	def getPerpendicularProfile(self, pos,direction,width,number_pts):
		(lon,lat)=pos
		elev=[]

		perpendicular=(direction+(1.5*pi))%(2*pi) # turn 90deg to left
		(vectx,vecty)=(cos(perpendicular),sin(perpendicular))

		#print (vectx,vecty)
		
		step=width*1.0/number_pts

		for l in ((width/2.0)-(step*i)-0.5*step for i in range(number_pts)):
			ex=lon+(vectx*l)
			ey=lat+(vecty*l)
			elev.append(float(self.__valueAt((ex,ey))))
		
		return elev

	def __valueAt(self, pos):
    		pp = self.__mapToPixel(pos)
    		x = int(pp[0]-self.offset[0])
    		y = int(pp[1]-self.offset[1])
		#x=int(pp[0])
		#y=int(pp[1])

		#print "value@%.3f %.3f = %d %d = %d %d"  %(pos[0],pos[1],pp[0],pp[1],x,y),

    		if x < 0 or y < 0 or x >= self.dem.shape[1] or y >= self.dem.shape[0]:
			raise Exception()
		#print "%d" %(self.dem[y,x],)
		return self.dem[y, x]
		
	def __PixelToMap(self,pos): # return (lon,lat)!
		# from http://www.gdal.org/gdal_datamodel.html:
		#    Xgeo = GT(0) + Xpixel*GT(1) + Yline*GT(2)
		#    Ygeo = GT(3) + Xpixel*GT(4) + Yline*GT(5)
		gt=self.geotransform # to make notation shorter
		return (
			gt[0]+pos[0]*gt[1]+pos[1]*gt[2],
			gt[3]+pos[0]*gt[4]+pos[1]*gt[5]
		)


	def __mapToPixel(self,pos):
		#gt=self.invgeotransform
		#return (
		#	gt[0]+pos[0]*gt[1]+pos[1]*gt[2],
		#	gt[3]+pos[0]*gt[4]+pos[1]*gt[5]
		#)

		# -> http://www.wolframalpha.com/input/?i=solve+%22lon%22+%3D+a%2Bx*b%2By*c%2C+%22lat%22+%3D+d%2Bx*e%2By*f+for+x%2Cy
		gt=self.geotransform
		return (
			# -((-a*f+c*d+c*(-lat)+f*lon)/(c*e-b*f))
			-((-gt[0]*gt[5]+gt[2]*gt[3]+gt[2]*(-pos[1])+gt[5]*pos[0])/(gt[2]*gt[4]-gt[1]*gt[5])),
			-((gt[0]*gt[4]-gt[1]*gt[3]+gt[1]*pos[1]-gt[4]*pos[0])/(gt[2]*gt[4]-gt[1]*gt[5]))
		)

		

def main():
	import argparse
	parser = argparse.ArgumentParser(description='Description of your program')
	parser.add_argument('from',help="Where from would you like to travel?")
	parser.add_argument('to',help="… and where does your journey lead you?")
	args = vars(parser.parse_args())

	from routing import fetchRoute
	route=fetchRoute(args['from'],args['to'])
	if not route:
		print "Could not route from %s to %s." %(args['from'],args['to'])
		return
	em=ElevationModel("/home/christoph/Downloads/srtm-global/srtm.tif",route.extent)
	
	profiles=[]
	
	for p in route:
		(position,direction)=p
		#print p
		profile=em.getPerpendicularProfile(position,direction,.5,12)
		profiles.append(profile)
	
	#print profiles

if __name__ == '__main__':
        main()
