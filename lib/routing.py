#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  osm (YOURS) routing
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

import urllib2, json
from math import atan2,pi,asin,sqrt
from nomatim import geocode

class Route:
	def __init__(self,geojson,padding=2):
		self.geojson=geojson
		#self.coordinates=((lat,lon) for [lon,lat] in geojson[u'coordinates'])
		self.coordinates=(
			(
				(geojson[u'coordinates'][i][0],geojson[u'coordinates'][i][1]),
				(asin2(geojson[u'coordinates'][i+1][0]-geojson[u'coordinates'][i][0],geojson[u'coordinates'][i+1][1]-geojson[u'coordinates'][i][1])+2*pi)%(2*pi)  # making sure angle is positive
			) for i in range(len(geojson[u'coordinates'])-1)
		)
		# get min and max lat and lon -> extents ;-)
		lats=list((lat for [lon,lat] in geojson[u'coordinates']))
		lons=list((lon for [lon,lat] in geojson[u'coordinates']))
		self.extent=(min(lons)-padding,min(lats)-padding,max(lons)+padding,max(lats)+padding)   # add 5deg on each side to accomodate elev profile safely
		lats=None
		lons=None

	def __iter__(self):
		return iter(self.coordinates)
		

def fetchRoute(fromplace,toplace):
	(flat,flon)=geocode(fromplace)
	(tlat,tlon)=geocode(toplace)
	print "Routing from %s (%.4f,%.4f) to %s (%.4f,%.4f)" %(fromplace,flat,flon,toplace,tlat,tlon)

	yours=urllib2.urlopen("http://www.yournavigation.org/api/1.0/gosmore.php?flat=%f&flon=%f&tlat=%f&tlon=%f&format=geojson&instructions=0" %(flat,flon,tlat,tlon))
	#print(yours)
	route=json.loads(yours.read())
	yours.close()
	
	if len(route[u'coordinates'])<3:
		return False
	return Route(route)

def asin2(cosinus,sinus): # x,y
	vector_length=sqrt(cosinus**2+sinus**2)
	try: 
		angle=asin(sinus/vector_length)
	except ZeroDivisionError:
		angle=0
	if cosinus<0:
		angle*=-1 # 3rd/4th quadrant
	angle=(angle+(2*pi))%(2*pi) # make positive
	return angle
	
	

def main():
	#route=fetchRoute("Salzburg","Köln")
	#for point in route:
	#	print point
	pass

if __name__ == '__main__':
        main()
