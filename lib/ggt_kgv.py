#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  ggT, kgV
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

def ggT(zahl1,zahl2):
	dividend=max(zahl1,zahl2) # groessere zahl
	divisor=min(zahl1,zahl2) # kleinere zahl

	rest=dividend % divisor

	while rest!=0:
		dividend=divisor
		divisor=rest
		rest=dividend%divisor

	return divisor

def kgV(zahl1,zahl2):
	return (zahl1*zahl2)/ggT(zahl1,zahl2)

def main():
	pass

if __name__ == '__main__':
        main()
