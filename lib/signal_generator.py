#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  signal-generator -> computes combined samples from lists of frequencies
#                      and amplitudes
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
from wavefile_handler import WaveFile
from math import sin,pi

from equaliser import Equaliser
equaliser=Equaliser()

def sinus_value(freq=440,offset=0,framerate=44100):
	#return sin(2.0*pi*(freq)/framerate) #+offset)
	pos=(float(offset)/float(framerate))%(2.0*pi)
	return sin(2.0*pi*float(freq)*pos) #(float(offset)/float(framerate)))

def compute_sample(frequencies=[],amplitudes=[],offset=0):
	#print frequencies,len(frequencies)
	#print amplitudes,len(amplitudes)
	if type(frequencies) != list or type(amplitudes) != list or len(frequencies) != len(amplitudes):
		raise Exception
	
	# normalise total amplitude (=volume) to 1
	#try: 
	#	amp_corr=1.0/sum(amplitudes)
	#except ZeroDivisionError:
	#	amp_corr=1.0

	for i in range(len(frequencies)):
		amplitudes[i]=equaliser.adjust(frequencies[i],amplitudes[i])

	amp_sum=sum(amplitudes)
	if amp_sum==0:
		amp_corr=1.0
	else:
		amp_corr=1.0/amp_sum
	amplitudes=list((a * amp_corr for a in amplitudes))
	#print (' '.join(("%01.5f" %(a,) for a in amplitudes)))
	#print (' '.join(("% 7d" %(f,) for f in frequencies)))

	try:
		# generate sine value for each frequency/amplitude pair, sum them up
		return sum((sinus_value(freq=frequencies[i],offset=offset)*amplitudes[i] for i in range(len(frequencies))))
	except:
		print "Error generating sine signal"
		print amplitudes
		sys.exit(-1)

def compute_n_samples(frequencies=[],amplitudes=[],offset=0,n_samples=1):
	return (compute_sample(frequencies,amplitudes,offset+i) for i in range(offset,offset+n_samples))

def main():
	wavefile=WaveFile("test.wav")
	samples=compute_n_samples([440.0],[0.7],1,441000)
	samples=((v,v) for v in samples)
	wavefile.write_samples(samples)
	wavefile=None



if __name__ == '__main__':
        main()
