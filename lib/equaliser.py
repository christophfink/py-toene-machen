#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  equaliser - applies correction factor to audio freq/amplitude pairs
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#

import operator
from math import log

def _log(dB):
	if dB > 0:
		return log(dB)+1 # that does not look correct … hm have to look it up somewhere
	elif dB==0:
		return 1.0
	else:
		return 1/log(abs(dB))

class Equaliser:
	def __init__(self,
		 equ=(			# values from http://www.mp3wirelessjukebox.com/media2/Equalizer.jpg
			(   60,	12),
			(  170,	 5),
			(  310,	 0),
			(  600,	 0),
			( 1000,	 5),
			( 3000,	 2),
			( 6000,	 0),
			(12000,	 9),
			(14000,	12),
			(16000,	18)
		)
	):
		self.equ=list((f,_log(a)) for (f,a) in sorted(equ,key=operator.itemgetter(0))) # sort from lowest to highest freq, convert dB to multipliers
		

	def __del__(self):
		pass # blah

	def adjust(self,frequency,amplitude):
		if frequency <= self.equ[0][0]:
			return amplitude*self.equ[0][1]
		elif frequency >=self.equ[-1][0]:
			return amplitude*self.equ[-1][1]
		else:
			pos=max(list(f for f in range(len(self.equ)) if self.equ[f][0]<=frequency )) # index of next-smallest self.equ
			low=self.equ[pos]
			high=self.equ[pos+1]
			delta=(frequency-low[0])/(high[0]-low[0])

			return amplitude*(low[1]+delta*(high[1]-low[1]))


#lut_equ={}
#for i in range(1,self.equ[0][0]):
#	print i
#	lut_equ[i]=log(equ[0][1]) # below min-val: = min-val
#for i in range(len(equ)-1):
#	lut_equ[equ[i][0]]=log(equ[i][1])
#	# linear interpolation
#	dist_freq=float(equ[i+1][0]-equ[i][0])
#	diff_dB=float(equ[i+1][1]-equ[i][1])
#	for j in range(int(dist_freq)+1):
#		print equ[i][0],j,equ[i][1],(j/dist_freq),diff_dB
#		print equ[i][1]+(j/dist_freq)
#		lut_equ[equ[i][0]+j]=\
#			log(equ[i][1]+\
#			(j/dist_freq)\
#			*diff_dB)
#for i in range(equ[-1][0],25000): # let's assume we can hear until 25k
#	lut_equ[i]=log(equ[-1][1])
#
#print lut_equ
#
#	
#
#def equaliser(frequency,amplitude): # values taken from http://www.mp3wirelessjukebox.com/media2/Equalizer.jpg
#	#if frequency<60:
#	#	amplitude*=log(12)  # +12dB
#	#elif frequency<170:
#	#	amplitude*=log(5)   # + 5dB
#	#elif frequency<310:
#	#	amplitude
#	pass
		

def main():
	equaliser=Equaliser()

	from musical_notes import notes
	for n in notes:
		print (n.frequency,equaliser.adjust(n.frequency,1))

	pass

if __name__ == '__main__':
        main()
