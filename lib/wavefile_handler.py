#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  wavefile handler -> converts samples into pcm frames and saves them to a file
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

import urllib2, json
from math import atan2,pi
from nomatim import geocode
import wave,struct

class WaveFile:
	def __init__(self,filename,nframes=None,nchannels=2,sampwidth=2,framerate=44100):
		self.filename=filename
		if nframes is None:
			nframes=0
		self.wavefile=wave.open(filename,'w')
		#self.wavefile.setparams((nchannels,sampwidth,framerate,nframes,'NONE','not compressed'))
		self.wavefile.setsampwidth(sampwidth)
		self.wavefile.setframerate(framerate)
		self.wavefile.setnchannels(nchannels)
		self.wavefile.setnframes(0) # random number
		#self.wavefile.close()
		#raise Exception # stop here for debugging reasons
		self.max_amplitude=float(int((2**(sampwidth*8))/2)-1)
		#print self.max_amplitude
		
	def __del__(self):
		self.wavefile.close()

	def generate_frames(self,samples,amplitude=1.0):
		#for s in samples:
		#	for c in s:
		#		if c>=1.0:
		#			print c
		return ''.join(''.join(struct.pack('h',int(self.max_amplitude*amplitude*sample))for sample in channels) for channels in samples)
        	#frames=''
        	#for s in samples:
                #	for c in s: # channels!
                #        	frames+=struct.pack('h', int(self.max_amplitude*c))
		#		print int(self.max_amplitude*c),
		#return frames

	def write_samples(self,samples):
		#print samples
		#print self.generate_frames(samples)
		self.wavefile.writeframesraw(self.generate_frames(samples))
		

def main():
	#route=fetchRoute("Salzburg","Köln")
	#for point in route:
	#	print point
	pass

if __name__ == '__main__':
        main()
